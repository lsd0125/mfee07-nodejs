class Person {
    constructor(name='bill', age=23) {
        this.name = name;
        this.age = age;
    }
    toJSON(){
        return JSON.stringify({
            name: this.name,
            age: this.age,
        })
    }
}

module.exports = Person;